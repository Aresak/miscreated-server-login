<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 04.02.2018
 * Time: 20:46
 */

// Start a session for saving the user's logins
session_start();

// Require the library to load
require_once "Account.php";

// This is my example account
$username   = "Aresak";
$password   = "myPassword";
$email      = "tmalcanek@symbiant.cz";







// Always check if the user is already logged in...
// ...first load the account from session
$account    = \Aresak\Account::GetAccountFromSession();
// If the user is already logged in, it will load his account into the $account
// Otherwise it will be null

// This is how you actually check if the user is logged in...
if($account == null) {
    // The account is null, which means the user is not logged in
    echo "Not logged in!<br>";
} else {
    // The account is not null, which means the user is logged in
    echo "Username: " . $account->Username() . "<br>"
        . "Email: " . $account->Email() . "<br>";

    // Also shown how you obtain the user's data
}







// I will register a new account now
$rand       = rand(0, 1000);        // I will just create an random number, to have unique username
$username   = $username . $rand;    // ...just add the number at the end

// The register function is here...
if($account->Register($username, $password, $email)) {
    // If the return from register is TRUE that means the registration has been a success
    echo "Registered user " . $username . "<br>";
}
else {
    // If the return is FALSE that means there is duplicate username
    echo "Username " . $username . " has been already registered.<br>";
}









// I will login with my new account now
if($account->CreateFrom_Login($username, $password)) {
    // If the return is TRUE, that means the username and password was valid
    echo "Logged in " . $username . "<br>";
} else {
    // If the return is FALSE, that means the username or password is wrong
    echo "Wrong username or password!<br>";
}






// I will logout now
$account->Logout();







// And because I want my users roaster I will pull all users
$allAccounts     = \Aresak\Account::GetAllMembers();

// Just loop through all of the accounts
foreach($allAccounts as $singleAccount) {
    // Do this with each account
    echo "ID: " . $singleAccount->ID() . ", Username: " . $singleAccount->Username() . ", Email: " . $singleAccount->Email() . "<br>";
}