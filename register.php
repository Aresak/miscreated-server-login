<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 05.02.2018
 * Time: 15:09
 */


// Start session
session_start();

// Include the Account library
require_once "Account.php";

// Get SQL connection
$sql        = \Aresak\General::SQL();
// Get account from session
$account    = \Aresak\Account::GetAccountFromSession();

// Check if logged in or not
if($account != null) {
    // The user is logged in
    header("Location: logged.php");
}

if(isset($_POST["register"])) {
    // Register account
    $account    = new \Aresak\Account($sql);
    $registered = $account->Register($_POST["username"], $_POST["password"], $_POST["email"]);
    if(!$registered)
        echo "<script>alert('The username is already taken');</script>";
    else
        header("Location: login.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SB Admin - Start Bootstrap Template</title>
    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
<div class="container">
    <div class="card card-register mx-auto mt-5">
        <div class="card-header">Register an Account</div>
        <div class="card-body">
            <form method="post">
                <input type="hidden" name="register" value="true">
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="username">Name</label>
                            <input class="form-control" id="username" name="username" type="text" aria-describedby="nameHelp" placeholder="Enter first name">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input class="form-control" id="email" type="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="password">Password</label>
                            <input class="form-control" id="password" name="password" type="password" placeholder="Password">
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-block">Register</button>
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="login.php">Login Page</a>
                <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>

