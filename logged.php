<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 05.02.2018
 * Time: 15:11
 */

// Start session
session_start();

// Include the Account library
require_once "Account.php";

// Get SQL connection
$sql        = \Aresak\General::SQL();
// Get account from session
$account    = \Aresak\Account::GetAccountFromSession();

// Check if logged in or not
if($account == null) {
    // The user is NOT logged in
    header("Location: login.php");
}

if(isset($_GET["logout"])) {
    // Logout
    $account->Logout();
    header("Location: login.php");
}

echo "Hi " . $account->Username() . ", your email is " . $account->Email();
echo "<br><a href='?logout'>Logout now</a>";