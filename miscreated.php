<?php

/**
 *
 *  Class which wraps the Discord Webhook and sends the
 *  notification about whitelist request into the discord channel.
 *
 */
class DiscordNotif {
    private $fname, $age, $discord, $steamid,
            $bio, $good, $kos, $agree;

    private $hook   = "";


    /**
     *
     *  Constructs the notification class by using known values.
     *  @param string   $fname      The user's first name
     *  @param int      $age        The user's age
     *  @param string   $discord    The user's discord username with discriminator
     *  @param string   $steamid    The user's steam ID
     *  @param string   $bio        The character's bio
     *  @param string   $good       Answer to: What is the Good Samaritan rule
     *  @param string   $kos        Answer to: What is KOS
     *  @param boolean  $agree      Does the user agree to server rules
     *
     */
    function __construct($fname, $age, $discord, $steamid, $bio, $good, $kos, $agree)
    {
        $this->fname    = $fname;
        $this->age      = $age;
        $this->discord  = $discord;
        $this->steamid  = $steamid;
        $this->bio      = $bio;
        $this->good     = $good;
        $this->kos      = $kos;
        $this->agree    = $agree;
    }


    /**
     *
     *  Sends the built message into the discord channel
     *  @return string The result from the command
     *
     */
    public function send() {

        $d = explode('#', $this->discord);
        $msg =    "**Character Name:** " . $this->fname . "\n"
                . "**Age:** " . $this->age . "\n"
                . "**Discord Username:** " . $this->discord . "\n"
                . "**SteamID:** " . $this->steamid . "\n"
                . "**Chracter Bio:** " . $this->bio . "\n"
                . "**What is the Good Samaritan Rule?:** " . $this->good . "\n"
                . "**What is KOS?:** " . $this->kos . "\n"
                . "**Agree to Server Rule:** " . ($this->agree ? "Yes" : "No") . "\n\n"
                . "Accept the request at: ****";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->hook);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "content=$msg");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $o = curl_exec ($ch);
        curl_close($ch);
        return $o;

    }
}



$notif = new DiscordNotif("Joe Joe", 20, "Aresak#1837", "1234567654321", "Some good bio here uluru", "i dont know urulu", "shooting aiden on sight", true);
var_dump($notif->send());
