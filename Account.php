<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 04.02.2018
 * Time: 19:18
 */
namespace Aresak;

class General {
    /** @var string The database host */
    private static $database_Host = "";

    /** @var string The database login username */
    private static $database_User = "";

    /** @var string The database login password */
    private static $database_Pass = "";

    /** @var string The database actual database name */
    private static $database_Datb = "";



    /**
     *  Creates an MySQLi connection
     *  @return \mysqli
     */
    public static function SQL() {
        $sql    = mysqli_connect(General::$database_Host, General::$database_User, General::$database_Pass, General::$database_Datb)
            or die(mysqli_error($sql));

        return $sql;
    }

    /**
     * Get an output from a result field and row
     * @param $result
     * @param $row
     * @param int $field
     * @return Mixed
     */
    public static function mysqli_result($result, $row, $field = 0)
    {
        if ($result === false) return false;
        if ($row >= mysqli_num_rows($result)) return false;
        if (is_string($field) && !(strpos($field, ".") === false)) {
            $t_field = explode(".", $field);
            $field = -1;
            $t_fields = mysqli_fetch_fields($result);
            for ($id = 0; $id < mysqli_num_fields($result); $id++) {
                if ($t_fields[$id]->table == $t_field[0] && $t_fields[$id]->name == $t_field[1]) {
                    $field = $id;
                    break;
                }
            }
            if ($field == -1) return false;
        }
        mysqli_data_seek($result, $row);
        $line = mysqli_fetch_array($result);
        return isset($line[$field]) ? $line[$field] : false;
    }

    /**
     *  Generate a random string by length
     *  @param int $length The length of the string
     *  @return string
     */
    public static function GenerateString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     *  Encrypt an message with a key
     *  @param string $data The data that needs to be encrypted
     *  @param string $key  The encryption key
     *  @return string
     */
    public static function encrypt($data, $key){
        return base64_encode(
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_128,
                $key,
                $data,
                MCRYPT_MODE_CBC,
                "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
            )
        );
    }

    /**
     *  Decrypt an message with a key
     *  @param string $data The encrypted data that needs to be decrypted
     *  @param string $key  The decryption key
     *  @return string
     */
    public static function decrypt($data, $key){
        $decode = base64_decode($data);
        return mcrypt_decrypt(
            MCRYPT_RIJNDAEL_128,
            $key,
            $decode,
            MCRYPT_MODE_CBC,
            "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
        );
    }
}

/**
 *  Class which works with the Accounts in the database
 *  @version    1.1
 *  @author     Aresak
 */
class Account
{
    /** @var int The ID of the user in the database */
    private $id;

    /** @var string The user's name in the database */
    private $username;

    /** @var string The user's hashed password in the database */
    private $password;

    /** @var string The user's registered email */
    private $email;

    /** @var \mysqli The connection to the database */
    private $sql;

    /** @var boolean Whether the Account object is initialized */
    private $initialized = false;

    /**
     *  Constructs an raw and empty Account object to work with.
     *  @param  \mysqli $sql        The connection to the database.
     *  @see    General::SQL()      Use this to create the database connection.
     */
    function __construct($sql)
    {
        $this->sql  = $sql;
    }

    // Public Object Functions
    /**
     *  Returns the user's ID in the database.<br>
     *  <b>The object must be initialized.</b> <i>(Read-only)</i>
     *  @return int                 The ID of the user in the database.
     */
    public function ID() {
        return $this->id;
    }

    /**
     *  Returns whether this account object has been initialized.<br>
     *  <i>(Read-only)</i>
     *  @return boolean             Whether this object has been initialized.
     */
    public function Initialized() {
        return $this->initialized;
    }

    /**
     *  Gets or sets the username in the database.<br>
     *  If there is an <i>null</i> parameter 'username' then return the username,
     *  otherwise set the username and change it in the database.
     *  @param string $username     The new user's name.
     *  @return string|boolean      <ul>
     *                                  <li><b>string</b> - Return the username on get</li>
     *                                  <li><b>boolean</b> - Return the status of the change in the database on set</li>
     *                              </ul>
     *  @see Initialized
     */
    public function Username($username = null) {
        if($username == null) {
            // The request was to get the username
            return $this->username;
        }

        // The request was to set the username
        $this->username     = $username;
        $query              = "UPDATE `user` SET username='$username'
                              WHERE ID='" . $this->id . "'";
        $result             = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        return true;
    }

    /**
     *  Gets or sets the password in the database.<br>
     *  If there is an <i>null</i> parameter 'password' then return the password,
     *  otherwise set the password and change it in the database.
     *  @param string $password     The new raw string password.
     *  @return string|boolean      <ul>
     *                                  <li><b>string</b> - Return the password on get</li>
     *                                  <li><b>boolean</b> - Return the status of the change in the database on set</li>
     *                              </ul>
     *  @see Initialized
     */
    public function Password($password = null) {
        if($password == null) {
            // The request was to get the password
            return $this->password;
        }

        // The request was to set the password
        $password       = md5("aiden" . $password);
        $this->password = $password;
        $query          = "UPDATE `user` SET password='$password'
                          WHERE ID='" . $this->id . "'";
        $result         = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        return true;
    }

    /**
     *  Gets or sets the email in the database.<br>
     *  If there is an <i>null</i> parameter 'email' then return the email,
     *  otherwise set the email and change it in the database.
     *  @param string $email        The new user's email.
     *  @return string|boolean      <ul>
     *                                  <li><b>string</b> - Return the email on get</li>
     *                                  <li><b>boolean</b> - Return the status of the change in the database on set</li>
     *                              </ul>
     *  @see Initialized
     */
    public function Email($email = null) {
        if($email == null) {
            // The request was to get the email
            return $this->email;
        }

        // The request was to set the email
        $this->email    = $email;
        $query          = "UPDATE `user` SET email='$email'
                          WHERE ID='" . $this->id . "'";
        $result         = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        return true;
    }

    /**
     *  Initializes this object from an username and a password.<br>
     *  The basic login function.
     *  @param string $username     The user's name.
     *  @param string $password     The raw string password.
     *  @return boolean             Returns <b>true</b> on success, otherwise <b>false</b>.
     */
    public function CreateFrom_Login($username, $password) {
        $username       = mysqli_escape_string($this->sql, $username);
        $password       = mysqli_escape_string($this->sql, md5("aiden" . $password));

        $query          = "SELECT * FROM `user` WHERE username='$username' AND password='$password'";
        $result         = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        if(mysqli_num_rows($result) == 0)
            return false;

        $this->loadFromResult($result, 0);

        $_SESSION["uid"]    = $this->ID();
        setcookie("uid", $this->id, time() + (60*60*24*31*3), "/");
        return true;
    }

    /**
     *  Initializes this object from an user's ID.<br>
     *  Used for target initialization.
     *  @param int $id              The user's ID in the database.
     *  @return boolean             Returns <b>true</b> on success, otherwise <b>false</b>
     */
    public function CreateFrom_ID($id) {
        $query          = "SELECT * FROM `user` WHERE ID='$id'";
        $result         = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        if(mysqli_num_rows($result) == 0)
            return false;

        $this->loadFromResult($result, 0);
        return true;
    }

    /**
     *  Initializes this object from an user's email.<br>
     *  Used for newsletters mostly.
     *  @param string $email        The user's email
     *  @return boolean             Returns <b>true</b> on success, otherwise <b>false</b>
     */
    public function CreateFrom_Email($email) {
        $query          = "SELECT * FROM `user` WHERE email='$email'";
        $result         = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        if(mysqli_num_rows($result) == 0)
            return false;

        $this->loadFromResult($result, 0);
        return true;
    }

    /**
     *  Register a new user.
     *  @param string $username     The user's name
     *  @param string $password     The user's raw password
     *  @param string $email        The user's email
     *  @return boolean             If the register has been an success
     */
    public function Register($username, $password, $email) {
        $username       = mysqli_escape_string($this->sql, $username);
        $password       = mysqli_escape_string($this->sql, md5("aiden" . $password));
        $email          = mysqli_escape_string($this->sql, $email);

        $query          = "SELECT * FROM `user` WHERE username='$username'";
        $result         = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        if(mysqli_num_rows($result) != 0)
            return false;

        $insert         = "INSERT INTO `user` (username, password, email) VALUES ('$username', '$password', '$email')";
        $insertRes      = mysqli_query($this->sql, $insert)
            or die(mysqli_error($this->sql));

        return $this->CreateFrom_Email($email);
    }

    // Private Object Functions
    /**
     *  Loads a data from the database into this object.<br>
     *  <i>Is called only from the CreateFrom functions.</i>
     *  @param \mysqli_result $result   The MySQLi result of the user in the database
     *  @param int $index               The index of the account in the result
     *  @return void
     */
    private function loadFromResult($result, $index) {
        $this->id       = General::mysqli_result($result, $index, "ID");
        $this->username = General::mysqli_result($result, $index, "username");
        $this->password = General::mysqli_result($result, $index, "password");
        $this->email    = General::mysqli_result($result, $index, "email");
    }

    // Public Static Functions
    /**
     *  Returns a new objects of all members in the database.
     *  @return Account[]           The array of registered members
     */
    public static function GetAllMembers() {
        $query          = "SELECT * FROM `user`";
        $sql            = General::SQL();
        $result         = mysqli_query($sql, $query)
            or die(mysqli_error($sql));

        $accounts       = array();
        for($i = 0; $i < mysqli_num_rows($result); $i ++) {
            $account    = new Account($sql);
            $account->loadFromResult($result, $i);
            array_push($accounts, $account);
        }
        return $accounts;
    }

    /**
     *  Gets an account from the session.
     *  @return Account|null        If there is a saved session, return Account, otherwise null.
     */
    public static function GetAccountFromSession() {
        if(isset($_COOKIE["uid"])) {
            $account    = new Account(General::SQL());
            if(!$account->CreateFrom_ID($_COOKIE["uid"]))
                return false;
            return $account;
        } else
            return false;
    }

    /**
     *  Logout the user from the session.
     *  @return void
     */
    public static function Logout() {
        unset($_SESSION["uid"]);
        setcookie("uid", "", time() - 1, "/");
    }
}
